$dir=$args[0]
[string]$dirRoot=$dir[0] + $dir[1]

$disk=Get-CimInstance Win32_LogicalDisk | Where-Object {$_.DeviceID -eq "$dirRoot"}

[int]$prosentFull=($disk.FreeSpace/$disk.Size)*100

$antallFiler=(Get-ChildItem $dir -Recurse -File).count

$avgFilStoerellse=Get-ChildItem $dir -Recurse -File | Measure-Object -Property Length -average | select-object -Expand average

$storsteFilStoerellse=(Get-ChildItem $dir -Recurse -File | sort Length -descendin | select-object -first 1).Length

$sti=(Get-ChildItem $dir -Recurse -File | sort Length -descendin | select-object -first 1 | Select-Object Fullname).Fullname

write-output "Disken er $prosentFull % full"

write-output "Antall filer i mappen: $antallFiler"

write-output "gjennomsnittlig filstoerrelse er $avgFilStoerellse B"

write-output "Stoerste fil er: $sti $storsteFilStoerellse B"
