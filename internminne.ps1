param (
   [string]$procName
)
$procInfo = Get-Process $procName | Select-Object WS
$totMem = $(systeminfo | Select-String 'Total Physical Memory:').ToString().Split(':')[1].Trim() #Done

# Jeg ga opp paa aa faa prosessens mb som egen verdi
$tekst = $procName + ": " + $procInfo + " B brukt av " + $totMem
Write-output $tekst
