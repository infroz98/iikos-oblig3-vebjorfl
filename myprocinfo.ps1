while ($ans -ne 9) {
    Write-Output "Kommandoer: `n
    1 - Hvem er jeg og hva er navnet paa dette scriptet? `n
    2 - Hvor lenge er det siden siste boot? `n
    3 - Hvor mange prosesser og traader finnes? `n
    4 - Hvor mange context switch'er fant sted siste sekund? `n
    5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund? `n
    6 - Hvor mange interrupts fant sted siste sekund? `n
    9 - Avslutt dette scriptet"
    $input =  Read-Host "Velg en funksjon"
    switch ($input) {
        # Done
        1 {$tekst = "Jeg er " + $env:USERNAME + " og dette scriptet heter " + $($MyInvocation.MyCommand.Name)
            Write-Output $tekst
            }
        # Done
        2 {
            $os = Get-CimInstance win32_operatingsystem
            $uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
            $tekst = "Tid siden sist boot: `n" + $uptime.Days + "D " + $uptime.Hours + "T " + $uptime.Minutes + "M " + $uptime.Seconds + "S"
            Write-output $tekst
            }
        3 {
            $prosesser = Get-Process
            $traader = (Get-Process|Select-Object -ExpandProperty Threads).Count
            $tekst = "Antall prosesser " + $prosesser.count + "  Antall traader " + $traader
            Write-output $tekst
            }
        4 {
            $switches=$(Get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec
            $tekst = [float]$switches + " context switcher sist sekund"
            Write-Output $tekst
            }
        5 {
            $uTid = (gcim Win32_PerfFormattedData_Counters_ProcessorInformation).PercentUserTime
            $uTid | ForEach-Object {$sum += $_}
            $sum = [math]::Round([double]$sum / $uTid.Length, 2)
            $kTid = 100 - $sum
            Write-Output "$sum% usermode, $kTid% kernelmode"
            }
        6 {
            $interrupts=((Get-Counter -Counter "\processor(_total)\interrupts/sec").CounterSamples | Format-Table CookedValue -HideTableHeader | Out-String).Trim()
            $tekst = "Interupts siste sekund:" + $interrupts
            write-output $tekst
            }
        9 {
          exit
        }
    }
}
