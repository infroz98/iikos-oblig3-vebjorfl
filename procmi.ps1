param(
  # Tar imot masse parametre
  [int[]] $Ids
)

# Kjoer for hver prosess
foreach ($Id in $Ids){
  $virtuelt = ((Get-Process | Where-Object{$_.Id -eq $Id}).VM)
  $working = ((Get-Process | Where-Object{$_.Id -eq $Id}).WS)

  $mb = ($virtuelt / 1024 / 1024) # Fikser verdiene
  $kb = ($working / 1024)

  $date = (Get-Date).TofileTime()
  $name = $Id, $date, ".txt"

  $ofs = '_'

  ("Total bruk av virtuelt minne: " + $mb + "MB"),("Stoerrelse paa Working Set: " + $kb + "KB") | Out-File C:\Users\infroz\Desktop\iikos-oblig3-vebjorfl\$name
}
